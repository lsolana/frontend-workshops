export class TabModel {
  id: string;
  titleTranslationKey: string;
  gridParamValues?: any;
  columns?: any[];
  searchTriggered?: boolean;
}
