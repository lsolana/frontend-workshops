import { ProductsState } from './products/products.models';

export interface ApplicationState {
  products: ProductsState;
}
